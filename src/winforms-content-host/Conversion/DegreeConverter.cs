﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using winforms_content_host.Models;

namespace winforms_content_host.Conversion
{
    public class DegreeConverter
    {

        // I didn't double check if this is how DMS works, all that really matters
        // for the tutorial is I have a reversible conversion.

        public Degrees Convert(DegreesMinutesSeconds input)
        {
            double result = input.Degrees;
            result += input.Minutes / 60;
            result += input.Seconds / 60 / 60;

            return new Degrees() { Value = result };
        }

        public DegreesMinutesSeconds Convert(Degrees input)
        {
            double degrees = Math.Floor(input.Value);
            double remaining = input.Value - degrees;
            double minutes = Math.Floor(remaining * 60);
            remaining = remaining - (minutes / 60);
            double seconds = Math.Floor(remaining * 60 * 60);

            return new DegreesMinutesSeconds()
            {
                Degrees = degrees,
                Minutes = minutes,
                Seconds = seconds
            };
        }
    }
}
