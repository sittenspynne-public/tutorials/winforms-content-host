﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using winforms_content_host.Models;
using winforms_content_host.Views;

namespace winforms_content_host
{
    public partial class Form1 : Form
    {
        // I could have a List<UserControl> or List<IConverterView>, but since there
        // are only two views to manage it's simple enough to reference both.
        private DegreesView _degreesView;
        private DegreesMinutesSecondsView _dmsView;

        private UserControl _currentView;

        public Form1()
        {
            InitializeComponent();

            // I didn't put these on the designer because I don't like to put things
            // I will manipulate manually on the designer.
            _degreesView = new DegreesView();
            _dmsView = new DegreesMinutesSecondsView();
            Controls.AddRange(new Control[] { _degreesView, _dmsView });

            // This makes the controls fill the form.
            _degreesView.Dock = DockStyle.Fill;
            _dmsView.Dock = DockStyle.Fill;

            _degreesView.SwapRequested += HandleSwap;
            _dmsView.SwapRequested += HandleSwap;

            // Take care to make sure a view is initialized at startup.
            SetContent(_degreesView);
        }

        private void HandleSwap(object sender, EventArgs e)
        {
            // Check who sent the request and choose the next view based on that.
            // In a more complex implementation, I might make the views pass a parameter
            // or implement a method to indicate the right "next" one.
            if (sender == _degreesView)
            {
                SetContent(_dmsView);
            }
            else if (sender == _dmsView)
            {
                SetContent(_degreesView);
            }
        }

        private void SetContent(UserControl content)
        {
            // Keep track of the old view, if it exists.
            var oldView = _currentView;
            _currentView = content;

            // Swap the visibility of the views. Keep in mind the old view might actually be
            // null. (First run, more complicated scenarios, etc.)
            oldView?.Hide();
            _currentView.Show();
        }
    }
}
