﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using winforms_content_host.Conversion;
using winforms_content_host.Models;

namespace winforms_content_host.Views
{
    public partial class DegreesMinutesSecondsView : UserControl
    {
        private DegreesMinutesSeconds _value = new DegreesMinutesSeconds();
        public DegreesMinutesSeconds Value
        {
            get { return GetValue(); }
            set
            {
                SetValue(value);
            }
        }

        public DegreesMinutesSecondsView()
        {
            InitializeComponent();
        }

        private void PerformConversion()
        {
            try
            {
                var converter = new DegreeConverter();
                var result = converter.Convert(Value);
                UpdateResult(result);
            }
            catch (InvalidOperationException)
            {
                InvalidInput();
            }

        }


        private DegreesMinutesSeconds GetValue()
        {
            if (double.TryParse(txtDegrees.Text, out double degrees) &&
                double.TryParse(txtMinutes.Text, out double minutes) &&
                double.TryParse(txtSeconds.Text, out double seconds))
            {
                return new DegreesMinutesSeconds()
                {
                    Degrees = degrees,
                    Minutes = minutes,
                    Seconds = seconds
                };
            }

            throw new InvalidOperationException("Invalid input.");
        }

        private void SetValue(DegreesMinutesSeconds value)
        {
            var (degrees, minutes, seconds) =
                (value.Degrees.ToString(), value.Minutes.ToString(), value.Seconds.ToString());
            txtDegrees.Text = degrees;
            txtMinutes.Text = minutes;
            txtSeconds.Text = seconds;
        }
        
        private void UpdateResult(Degrees result)
        {
            lblResults.Text = $"{result.Value} degrees.";
        }

        private void InvalidInput()
        {
            lblResults.Text = "Invalid input.";
        }

        private void BtnConvert_Click(object sender, EventArgs e)
        {
            PerformConversion();
        }

        private void BtnSwap_Click(object sender, EventArgs e)
        {
            OnSwapRequested(EventArgs.Empty);
        }

        protected void OnSwapRequested(EventArgs e)
        {
            SwapRequested?.Invoke(this, e);
        }

        public event EventHandler SwapRequested;

    }
}
