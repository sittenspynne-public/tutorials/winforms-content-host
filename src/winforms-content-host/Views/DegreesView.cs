﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using winforms_content_host.Conversion;
using winforms_content_host.Models;

namespace winforms_content_host.Views
{

    // This could be far more complex in a bigger application, using events to
    // keep the controls and properties in sync with each other. For this tutorial
    // I am focusing on the swapping aspect, not "how to MVC".
    public partial class DegreesView : UserControl
    {

        public Degrees Value
        {
            get { return GetValue(); }
            set { SetValue(value); }
        }

        public DegreesView()
        {
            InitializeComponent();
        }

        private void PerformConversion()
        {
            try
            {
                // If the input can be converted, display the conversion result.
                var converter = new DegreeConverter();
                var result = converter.Convert(Value);
                UpdateResult(result);
            }
            catch (InvalidOperationException)
            {
                // If the input cannot be converted, display an error message.
                InvalidInput();
            }
        }

        // Update controls with this value.
        private void SetValue(Degrees value)
        {
            txtDegrees.Text = value.Value.ToString();
        }

        // Get a value from the controls, or throw if the input is invalid.
        private Degrees GetValue()
        {
            if (double.TryParse(txtDegrees.Text, out double degrees))
            {
                return new Degrees() {Value = degrees};
            }

            throw new InvalidOperationException("Invalid input.");
        }

        // Format the DMS conversion and display it. Sorry I was too lazy to look up the symbols.
        private void UpdateResult(DegreesMinutesSeconds converted)
        {
            var (degrees, minutes, seconds) = (converted.Degrees, converted.Minutes, converted.Seconds);
            lblResults.Text = $"{degrees} {minutes} {seconds}";
        }

        // Display an error message.
        private void InvalidInput()
        {
            lblResults.Text = "Invalid input.";
        }

        private void BtnConvert_Click(object sender, EventArgs e)
        {
            PerformConversion();
        }

        private void BtnSwap_Click(object sender, EventArgs e)
        {
            OnSwapRequested(EventArgs.Empty);
        }

        // The host form listens for this event to know when to change to
        // a different converter.
        protected void OnSwapRequested(EventArgs e)
        {
            SwapRequested?.Invoke(this, e);
        }

        public event EventHandler SwapRequested;
    }
}
